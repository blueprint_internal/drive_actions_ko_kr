
function Mobile_Ad(resources)
{
	Mobile_Ad.resources = resources;
	
}
Mobile_Ad.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(800, 500, Phaser.CANVAS, 'mobile_ad', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 500;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone', Mobile_Ad.resources.phone);
		this.game.load.image('line', Mobile_Ad.resources.line);

		
		for(var i = 0;i<5;i++)
		{
			cnt = i+1;
			this.game.load.image('icon_'+cnt, Mobile_Ad.resources['icon_'+cnt]);

		}
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.phone = this.game.add.sprite(this.game.world.centerX-110,this.game.world.centerY,'phone');
			
			this.parent.phone.anchor.set(0.5);
			this.parent.iconAr = [];
			this.parent.textAr = [];
			this.parent.lineAr = [];
			this.parent.textAnimAr = [];
			this.parent.lineAnimAr = [];
			var cnt = 0;
			this.parent.style = Mobile_Ad.resources.textStyle_1;
			this.game.load.image('icon_'+cnt, Mobile_Ad.resources[cnt+"_icon"]);
			for(var i = 0;i<5;i++)
			{
				cnt = i+1;
				var tempText = this.game.add.text(this.game.world.centerX+160,100+(100*i), Mobile_Ad.resources["icon_text_"+cnt],this.parent.style);
				tempText.id = cnt;
				tempText.inputEnabled = true;
				tempText.events.onInputDown.add(this.parent.actionOnClick, this);
				tempText.input.useHandCursor = true;
				tempText.input._setHandCursor = true;
				this.parent.textAr.push(tempText);
				this.parent.textAr[i].alpha = 0;

				var temp_button = this.game.add.button(this.game.world.centerX-400,110+(100*i),'icon_'+cnt,this.parent.actionOnClick,this);
				temp_button.id = cnt;
				this.parent.iconAr.push(temp_button);
				
				temp_button.useHandCursor = true;
				this.parent.iconAr[i].alpha = 0;
				this.parent.iconAr[i].anchor.set(0.5,0.5);
				
				
				//this.parent.textAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr.push(this.game.add.sprite(this.game.world.centerX+150,90+(80*i),'line'));
				this.parent.lineAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr[i].alpha = 0;
				this.parent.textAnimAr.push(this.game.add.tween(this.parent.textAr[i]).to({alpha:1,y:100+(70*i)},740,Phaser.Easing.Quadratic.Out));
				this.parent.lineAnimAr.push(this.game.add.tween(this.parent.lineAr[i]).to({alpha:1},500,Phaser.Easing.Quadratic.Out));


			}

			this.game.created  = true;
			
			this.parent.buildAnimation();
			
		}
	},

	actionOnClick: function(evt){
		
		this.parent.trigger("domPopup",{title:Mobile_Ad.resources['icon_text_'+evt.id],body:Mobile_Ad.resources['popupMessage_'+evt.id]});
	},
    
	buildAnimation: function()
	{
		this.lineAnimAr[0].chain(this.lineAnimAr[1],this.lineAnimAr[2],this.lineAnimAr[3],this.textAnimAr[0],this.textAnimAr[1],this.textAnimAr[2],this.textAnimAr[3]);
		this.lineAnimAr[0].start();
		
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}



